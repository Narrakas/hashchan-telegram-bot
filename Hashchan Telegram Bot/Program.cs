﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Hashchan_Telegram_Bot
{
    /// <summary>
    /// Main Bot
    /// </summary>
    class Program
    {
        /// <summary>
        /// Telegram Bot Client
        /// </summary>
        private static  TelegramBotClient Bot;

        /// <summary>
        /// Main Program Method
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            Bot = new TelegramBotClient(args[0]);
            var me = Bot.GetMeAsync().Result;
            Console.Title = me.Username;

            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            Bot.StartReceiving();
            Console.WriteLine($"Start listening for @{me.Username}");
            Console.ReadLine();
            Bot.StopReceiving();
        }

        /// <summary>
        /// When the bot receives a message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="messageEventArgs"></param>
        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null || message.Type != MessageType.Text) return;

            switch (message.Text.Split(' ').First())
            {
                // send inline keyboard
                case "/md5":
                    var input = message.Text.Replace(message.Text.Split(' ').First(), "").Substring(1);
                    string response = $"<code>{@Hashing.MD5.Calculate(input)}</code>";

                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                    await Task.Delay(500); // simulate longer running task
                    await Bot.SendTextMessageAsync(message.Chat.Id, response, parseMode: ParseMode.Html, replyMarkup: new ReplyKeyboardRemove());
                    break;
                    
                default:
                    const string usage = @"
Available algorithms:
/md5";

                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        usage,
                        replyMarkup: new ReplyKeyboardRemove());
                    break;
            }
        }
        
        /// <summary>
        /// When the bot receives an error
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiveErrorEventArgs"></param>
        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Received error: {0} — {1}",
                receiveErrorEventArgs.ApiRequestException.ErrorCode,
                receiveErrorEventArgs.ApiRequestException.Message);
        }
    }
}
